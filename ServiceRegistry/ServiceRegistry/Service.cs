﻿namespace ServiceRegistry
{
    class Service
    {
        protected bool Equals(Service other)
        {
            return Id == other.Id && string.Equals(Name, other.Name) && string.Equals(Address, other.Address);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Service) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Id;
                hashCode = (hashCode*397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Address != null ? Address.GetHashCode() : 0);
                return hashCode;
            }
        }

        public int Id;
        public string Name;
        public string Address;

        public static void CopyTo(Service source, Service target)
        {
            target.Name = source.Name;
            target.Id = source.Id;
            target.Address = source.Address;
        }
    }
}
