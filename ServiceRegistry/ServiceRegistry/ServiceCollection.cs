﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.Linq;

namespace ServiceRegistry
{
    class ServiceCollection : List<Service>
    {
        SQLiteConnection connection;
        const string databaseName = @"service.db";

        public ServiceCollection()
        {
            connection = new SQLiteConnection(string.Format("Data Source={0};", databaseName));
        }

        public void Reload()
        {
            connection.Open();
            SQLiteCommand command = new SQLiteCommand($"SELECT * FROM service;", connection);
            SQLiteDataReader reader = command.ExecuteReader();


            foreach (DbDataRecord record in reader)
            {
                Service service = new Service
                {
                    Id = Convert.ToInt32(record["id"]),
                    Name = record["name"].ToString(),
                    Address = (record["address"]).ToString(),
                };

                var serviceExists = this.FirstOrDefault(service1 => service1.Name.Equals(service.Name));
                if (serviceExists != null)
                {
                    if (!serviceExists.Equals(service))
                    {
                        Service.CopyTo(service, serviceExists);
                    }
                }
                else
                {
                    Add(service);
                }
            }
            connection.Close();

        }

        public Service GetByName(string name)
        {
            return this.FirstOrDefault(service1 => service1.Name.Equals(name));
        }

        public void AddDb(Service service)
        {
            try
            {
                connection.Open();
                SQLiteCommand command = new SQLiteCommand($"insert into service values({service.Id},'{service.Name}','{service.Address}');", connection);
                command.ExecuteScalar();
                connection.Close();

                Reload();
            }
            catch (Exception)
            {
              
            }
         
        }

        public void Update(Service service)
        {
            connection.Open();
            SQLiteCommand command = new SQLiteCommand($"UPDATE service set address = '{service.Address}' WHERE name = '{service.Name}';", connection);
            command.ExecuteScalar();
            connection.Close();

            Reload();
        }
    }
}

