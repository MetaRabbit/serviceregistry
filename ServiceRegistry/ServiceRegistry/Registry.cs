﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ServiceRegistry
{
    class Registry : IDisposable
    {

        //Массив для отправки широковещательной рассылки при смене ip одного из сервера
        List<string> _serverMetadataBroadcast;
        public ServiceCollection ServiceCollection;
        ConnectionFactory _factory;
        Dictionary<int, Action<string>> _tableHandlers;
        IModel _model;
        IConnection _connection;
        EventingBasicConsumer registryConsumer;

        public Registry()
        {
            ServiceCollection = new ServiceCollection();
            _factory = new ConnectionFactory() { Endpoint = new AmqpTcpEndpoint("188.134.14.125", 5748), UserName = "dimcaster", Password = "q1w2e3r4t5y6", VirtualHost = "botting" };
            _tableHandlers = new Dictionary<int, Action<string>>();
            _tableHandlers.Add(0, HandleAddressRequest);
            _tableHandlers.Add(1, HandleRegisterService);
            _connection = _factory.CreateConnection();
            _model = _connection.CreateModel();
        }


        public void Update(Service service)
        {
            try
            {
                ServiceCollection.Update(service);
                Fanout(service);
            }
            catch (Exception)
            {
                
               
            }
           
        }

        private void Fanout(Service service)
        {

            _model.ExchangeDeclare("servers_address_events_change", "fanout");

            JObject jObject = new JObject();
            jObject.Add("AuthServerAddress", ServiceCollection.GetByName("AuthServerAddress").Address);
            jObject.Add("BotMonitorAddress", ServiceCollection.GetByName("BotMonitorAddress").Address);

             var body = Encoding.UTF8.GetBytes(jObject.ToString());
            _model.BasicPublish(exchange: "servers_address_events_change",
                                     routingKey: "",
                                     basicProperties: null,
                                     body: body);
            }


        public void Listen()
        {
            _model.ExchangeDeclare("system", "direct");
            var d = _model.QueueDeclare(queue: "service_registry_queue",
                                durable: false,
                                exclusive: false,
                                autoDelete: false,
                                arguments: null);
            Console.WriteLine(d.MessageCount);
           
            _model.QueueBind(queue: "service_registry_queue",
                  exchange: "system",
                  routingKey: "service_registry");


            registryConsumer = new EventingBasicConsumer(_model);
            registryConsumer.Received += (model, ea) =>
            {
                var body1 = ea.Body;
               
                try
                {
                    JObject jObject = JObject.Parse(Encoding.UTF8.GetString(body1));
                    _tableHandlers[jObject["id"].Value<int>()].Invoke(jObject["body"].Value<string>());
                }
                catch (Exception e)
                {
                    Console.WriteLine("Невозможно обработать сообщение из очереди! : " + " " + e.Message);                    
                }

              
            };
            _model.BasicConsume(queue: "service_registry_queue",
                               noAck: true,
                               consumer: registryConsumer);
        }


        void ResponseAddressSettings(string idBot ,string nameExchange)
        {
            JObject jObject = new JObject();
            jObject.Add("AuthServerAddress", ServiceCollection.GetByName("AuthServerAddress").Address);
            jObject.Add("BotMonitorAddress", ServiceCollection.GetByName("BotMonitorAddress").Address);
            var body = Encoding.UTF8.GetBytes(jObject.ToString());
            _model.BasicPublish(exchange: nameExchange,
                                     routingKey: "BotUpdater#" + idBot,
                                     basicProperties: null,
                                     body: body);

        }

        #region Handlers


        void HandleAddressRequest(string body)
        {
            JObject jObject = JObject.Parse(body);
            ResponseAddressSettings(jObject["id"].Value<string>(), jObject["botName"].Value<string>());

        }

        void HandleRegisterService(string body)
        {         
            //Добавление если не существует
            ServiceCollection.AddDb(JsonConvert.DeserializeObject<Service>(body));
            //Обновление
        }
        #endregion

        public void Dispose()
        {
            _model.Dispose();
            _connection.Dispose();
            
        }
    }
    
}
