﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading;
using MarkdownLog;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ServiceRegistry
{
    class Program : CriticalFinalizerObject
    {
        static List<string> serverMetadataBroadcast = new List<string>();
        static ServiceCollection _serviceCollection;
        public static object _lock = new object();
        static Registry _registry;

        static void Main(string[] args)
        {
            _registry = new Registry();
            //Загрузка данных из бд
            _registry.ServiceCollection.Reload();
            //Начинаем слушать сообщения из пути реестра сервисов
            _registry.Listen();
            Console.WriteLine("Команды ");
            Console.WriteLine("list  - список сервисов");
            Console.WriteLine("s <name> <address> - смена адреса сервиса по имени");
            Console.WriteLine("add <name> <id> <address> - добавление нового сервиса");
            Console.WriteLine("Ожидание команды");
            //Начало прослуши консоли
            while (true)
            {

                Console.WriteLine("Ожидание команды");

                string command  = Console.ReadLine();
                string[] argumets = command.Split(' ');

                switch (argumets[0])
                {
                    case "list":
                    {

                            Console.WriteLine(
                            _registry.ServiceCollection.ToList().Select(s => new
                            {
                                Id = s.Id,
                                Name = s.Name,
                                Address = s.Address,
                            })
                              .ToMarkdownTable());

                            break;
                    }
                    case "s":
                    {
                        var service = _registry.ServiceCollection.GetByName(argumets[1]);
                        if (service == null)
                        {
                            Console.WriteLine("Сервис не найден");
                            continue;
                        }
                        service.Address = argumets[2];
                        lock (_lock)
                        {
                            _registry.Update(service);                          
                        }
                           
                        break;
                    }

                    case "add":
                        {
                            var service = new Service() {Name = argumets[1], Id = Convert.ToInt32(argumets[2]), Address = argumets[3] };
                            lock (_lock)
                            {
                                _registry.ServiceCollection.AddDb(service);
                            }

                            break;
                        }

                }



            }




            ConnectionFactory factory = new ConnectionFactory() { Endpoint = new AmqpTcpEndpoint("188.134.14.125", 5748), UserName = "dimcaster", Password = "q1w2e3r4t5y6", VirtualHost = "botting" };


            IConnection conn = factory.CreateConnection();

            using (var channel = conn.CreateModel())
            {


                var consumer = new EventingBasicConsumer(channel);


                channel.ExchangeDeclare("server_metadata_broadcast", "fanout");
                channel.QueueDeclare(queue: "hello",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                string message = "Hello World!";
                var body = Encoding.UTF8.GetBytes(message);
                channel.BasicPublish(exchange: "",
                                     routingKey: "hello",
                                     basicProperties: null,
                                     body: body);
                Console.WriteLine(" [x] Sent {0}", message);
            }

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }

        ~Program()
        {
            _registry.Dispose();
        }
    }
}
